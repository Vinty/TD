using UnityEngine;

public class Enemy : MonoBehaviour {
	[SerializeField] private Transform   exit;
	[SerializeField] private Transform[] waypoints;
	[SerializeField] private float       navigation;

	private Transform enemy;
	private float     navigationTime = 0;
	private int       target         = 0;

	void Start() {
		enemy = GetComponent<Transform>();
		Manager.Instance.RegisterEnemy(this);
	}


	void Update() {
		if(waypoints == null) {
			return;
		}

		navigationTime += Time.deltaTime;
		if(navigationTime < navigation) {
			return;
		}

		Vector3 positionTarget = target < waypoints.Length ? waypoints[target].position : exit.position;
		enemy.position = Vector2.MoveTowards(enemy.position, positionTarget, navigationTime);
		navigationTime = 0;
	}

	private void OnTriggerEnter2D(Collider2D collision) {
		switch(collision.tag) {
			case"MovingPoint" :
				target++;
				break;
			case"Finish" :
				Manager.Instance.UnRegisterEnemy(this);
				break;
		}
	}
}
