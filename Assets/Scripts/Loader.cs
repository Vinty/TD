using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader<T> : MonoBehaviour where T : MonoBehaviour {
	private static T instance;

	public static T Instance {
		get {
			T foundObject = FindObjectOfType<T>();
			if(instance == null) {
				instance = foundObject;
			}
			else if(instance != foundObject) {
				Destroy(foundObject);
			}

			DontDestroyOnLoad(foundObject);
			return instance;
		}
	}
}
