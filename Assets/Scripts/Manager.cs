using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : Loader<Manager> {

	[SerializeField] private GameObject   spawnPoint;
	[SerializeField] private GameObject[] enemies;
	[SerializeField] private int          maxEnemiesInScreen;
	[SerializeField] private int          totalEnemies;
	[SerializeField] private int          enemiesPerSpawn;
	[SerializeField] private float        spawnDelay;
	
	public List<Enemy> enemyList = new List<Enemy>();

	private void Start() {
		StartCoroutine(Spawn(0));
	}

	private IEnumerator Spawn(int enemiesIndex) {
		if(enemiesPerSpawn > 0 && enemyList.Count < totalEnemies) {
			for(int i = 0; i < enemiesPerSpawn; i++) {
				if(enemyList.Count < maxEnemiesInScreen) {
					GameObject newEnemy = Instantiate(enemies[enemiesIndex]);
					newEnemy.transform.position = spawnPoint.transform.position;
				}
			}
			yield return new WaitForSeconds(spawnDelay);
			StartCoroutine(Spawn(enemiesIndex));
		}
	}

	public void RegisterEnemy(Enemy enemy) {
		enemyList.Add(enemy);
	}
	
	public void UnRegisterEnemy(Enemy enemy) {
		enemyList.Remove(enemy);
		Destroy(enemy.gameObject);
	}


	public void DestroyEnemies() {
		foreach(Enemy enemy in enemyList) {
			Destroy(enemy.gameObject);
		}
		
		enemyList.Clear();
	}
}
