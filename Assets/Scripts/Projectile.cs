using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ProjectileType {
	ROCK,
	ARROW,
	FIREBALL
}

public class Projectile : MonoBehaviour {
	[SerializeField] private int            attackDamage;
	[SerializeField] private ProjectileType pType;

	public int AttackDamage {
		get => attackDamage;
	}

	public ProjectileType PType {
		get => pType;
	}
}
