using Towers;
using UnityEngine;
using UnityEngine.EventSystems;

public class TowerManager : Loader<TowerManager> {
	private TowerButton    towerButtonPressed;
	private SpriteRenderer spriteRenderer;

	private void Start() {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	private void Update() {
		if(spriteRenderer.enabled) {
			FollowMouse();
		}
		if(!Input.GetMouseButtonDown(0)) {
			return;
		}

		RaycastHit2D hit = GetHit();
		if(!GetHit()) {
			return;
		}

		if(hit.collider.CompareTag("TowerSide")) {
			hit.collider.tag = "TowerSideFull";
			PlaceTower(hit);
		}
	}

	private RaycastHit2D GetHit() {
		Vector2 mousePoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		return Physics2D.Raycast(mousePoint, Vector2.zero);
	}

	public void PlaceTower(RaycastHit2D hit) {
		if(!EventSystem.current.IsPointerOverGameObject() && towerButtonPressed != null) {
			GameObject newTower = Instantiate(towerButtonPressed.TowerObj);
			newTower.transform.position = hit.transform.position;
			DisableDrag();
		}
	}

	public void SelectedTower(TowerButton towerSelected) {
		towerButtonPressed = towerSelected;
		EnableDrag(towerButtonPressed.DragSprite);
	}

	public void FollowMouse() {
		transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		transform.position = new Vector2(transform.position.x, transform.position.y);
	}

	public void EnableDrag(Sprite sprite) {
		spriteRenderer.enabled = true;
		spriteRenderer.sprite = sprite;
	}
	public void DisableDrag() {
		spriteRenderer.enabled = false;
	}
	
}
