using UnityEngine;

namespace Towers {
	public class TowerButton : MonoBehaviour {
		[SerializeField] private GameObject towerObj;
		[SerializeField] private Sprite     dragSprite;

		public GameObject TowerObj {
			get => towerObj;
		}

		public Sprite DragSprite {
			get => dragSprite;
		}
	}
}
