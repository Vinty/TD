using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Towers {
	public class TowerControl : MonoBehaviour {
		[SerializeField] private float timeBetweenAttack;
		[SerializeField] private float attackRadius;

		private Projectile projectile;
		private Enemy      targetEnemy = null;
		private float      attackCounter;


		private List<Enemy> GetEnemiesInRange() {
			return Manager.Instance.enemyList
			              .Where(enemy =>
				                     Vector2.Distance(transform.position, enemy.transform.position) <= attackRadius
			               )
			              .ToList();
		}

		private Enemy GetNearestEnemy() {
			Enemy nearestEnemy     = null;
			float smallestDistance = float.PositiveInfinity;

			foreach(Enemy enemy in GetEnemiesInRange()
			   .Where(enemy =>
				          Vector2.Distance(transform.position, enemy.transform.position) < smallestDistance
				)) {
				smallestDistance = Vector2.Distance(transform.position, enemy.transform.position);
				nearestEnemy     = enemy;
			}

			return nearestEnemy;
		}
	}
}
